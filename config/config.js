const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const env = process.env.NODE_ENV || 'development';

const config = {
  development: {
    root: rootPath,
    app: {
      name: 'patrulla'
    },
    port: process.env.PORT || 3000,
    db: process.env.DBHOST || 'mongodb://localhost/patrulla-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'patrulla'
    },
    port: process.env.PORT || 3000,
    db: process.env.DBHOST || 'mongodb://localhost/patrulla-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'patrulla'
    },
    port: process.env.PORT || 3000,
    db: process.env.DBHOST || 'mongodb://localhost/patrulla-production'
  }
};

module.exports = config[env];
