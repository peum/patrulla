# Patrulla

Este es un proyecto demo para ver las fases de construcción con gitlab + nodeJS + Test (Mocha) + CI + Docker + Cypress + ...

## Install and Run

$npm install

$docker-compose run --rm -p 27017:27017 mongodb

$gulp

## Cypress testing

### Using docker

$docker-compose run --rm cypress

### Using local installation

$npm install cypress --no-save

$docker-compose run --rm -p 3000:3000 patrulla

$npx cypress open --config baseUrl=http://localhost:3000/

# Try in PWD

[![Try in PWD](https://cdn.rawgit.com/play-with-docker/stacks/cff22438/assets/images/button.png)](http://play-with-docker.com?stack=https://gitlab.com/peum/patrulla/raw/master/stack.yml&stack_name=peum)
